package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/gin-gonic/gin"
)

type EnvironmentVariables struct {
	EnvironmentName string `json:"EnvironmentName"`
	Foo             string `json:"foo"`
}

type EnvironmentVariablesVault struct {
	Data     Data     `json:"data"`
	Metadata Metadata `json:"metadata"`
}

type Data struct {
	EnvironmentName string `json:"EnvironmentName"`
	Foo             string `json:"foo"`
}

type Metadata struct {
	CreatedTime    string      `json:"created_time"`
	CustomMetadata interface{} `json:"custom_metadata,omitempty"`
	DeletionTime   string      `json:"deletion_time,omitempty"`
	Destroyed      bool        `json:"destroyed"`
	Version        int64       `json:"version"`
}

func main() {
	router := gin.Default()
	// Plain JSON structure
	router.GET("/ping", func(ctx *gin.Context) {
		path := os.Getenv("CONFIGURATION_FILE_PATH")
		var environmentVariables EnvironmentVariables
		parseJsonFile(path, &environmentVariables)
		ctx.JSON(200, environmentVariables)
	})

	// Vault struct
	router.GET("/pong", func(ctx *gin.Context) {
		path := os.Getenv("CONFIGURATION_FILE_PATH")
		var environmentVariables EnvironmentVariablesVault
		parseJsonFile(path, &environmentVariables)
		ctx.JSON(200, environmentVariables)
	})
	router.Run()
}

func parseJsonFile(path string, body interface{}) {
	jsonFile, err := os.Open(path)

	if err != nil {
		panic(err)
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	err = json.Unmarshal(byteValue, &body)

	if err != nil {
		panic(err)
	}
}
