# STEP 1 build executable binary
FROM golang:alpine AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .
COPY . .

RUN go get -d -v

RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -ldflags="-w -s" -o main .

WORKDIR /app/build

RUN cp -r /app/main .

# STEP 2 build a small image
FROM scratch

WORKDIR /app

COPY --from=builder /app/build/* ./

ENV PORT 8080
ENV GIN_MODE release
EXPOSE 8080

ENTRYPOINT ["/app/main"]